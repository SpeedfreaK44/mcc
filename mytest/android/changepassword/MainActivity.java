package com.example.changepassword;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText oldp,newp,conp;
        final Button change;

        oldp = findViewById(R.id.oldp);
        newp = findViewById(R.id.newp);
        conp = findViewById(R.id.conp);
        change = findViewById(R.id.button);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context =  getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                if (oldp.getText().toString().equals("old") && newp.getText().toString().equals(conp.getText().toString()))
                {
                    Toast.makeText(context, "Password Changed Successfully", duration).show();
                }
                else
                {
                    Toast.makeText(context, "Error. Try Again", duration).show();
                }
            }
        });
    }
}
